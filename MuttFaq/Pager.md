### How to turn off "Command:" prompt when using an external pager?  What is it for?

This is controlled by the `$prompt_after` configuration variable.

The prompt allows doing things like toggling headers, or even switching
to the next/previous message, without needing to return to the index.

### How do I color part of headers, instead of the whole header?

`$header_color_partial` changes how "color header" matches are
applied.  If unset, the default, matches will color the entire
header.  When set, only the matched string will be colored. This, for
example, allows just header labels to be colored:

```
set header_color_partial
color header default green '^[^[:blank:]:]*:'
color hdrdefault yellow default
```
