### How to get rid of duplicate msgs?

Use the "~=" mutt-pattern [manual.txt](home) section
4.2) to delete or tag them. Beware: in old versions this pattern works
only when you sort the folder by "thread".

### How to change the order within a thread?

### How to sort threads based on all the messages in the thread?

RTFM **sort\_aux**, it controls not just the order of messages within
each thread, but also the order of threads themselves!

A good use for this is:

` sort_aux=last-date-received`

so that threads with messages received recently will show up later in
the index (by default threads are sorted based on the first message in
the thread).

### Why are some msgs threaded and others not?

You have some msgs which don't have correct

`In-Reply-To:`  
`References:`

headers (or not set at all) and you've turned on

`$strict_threads`

### What do "-\>", "-?-" and "\*\>" mean in thread trees?

When you turn off

`$strict_threads`

msgs with similar subjects get grouped together. In those threads "-\>"
indicates **real** threads (by the above mentioned headers) and "\*\>"
indicates **pseudo** threads (by subject only).

"-?-" indicates a referenced msg, which is missing in the current
folder.

### Why is the tree display screwed, I see garbage chars in front of the arrows?!

Try setting **$ascii\_chars** on. If the display is still messed up,
then you have a problem (i.e. flea). Otherwise the fancy tree chars
resulted from a bad combination of your used terminal emulation and/or
wrong $TERM setting in the shell, or maybe even $LC\_CTYPE or $LANG (see
"man locale"). See [MuttFaq/Charset](MuttFaq/Charset).
