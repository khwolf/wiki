This is a list of tips for upgrading to the latest version of mutt
(currently 1.6).

## Known Issues

  - There is a problem with the new idna code when mutt is compiled
    without iconv
support.

 Users without iconv support are advised not to upgrade until we release a bug fix release,  
 (1.6.1) in a few weeks.  Sorry about that.

## Upgrading from 1.5.x

  - First, be sure to take a look at the UPDATING document at
    https://gitlab.com/muttmua/mutt/raw/master/UPDATING

<!-- end list -->

  - Folder-hooks and mailbox-hooks apply mailbox shortcuts to the first
    character. Be sure to
review

 <https://muttmua.gitlab.io/mutt/manual-dev.html#mailbox-hook> if you are seeing a warning  
 `current mailbox shortcut '^' is unset`.

  - Several suggested GPG and S/MIME configuration options have changed
    over the 1.5.x
series.

 Most recently, $pgp_decryption_okay was added, and the $smime_sign_command was modified to include  
 a message digest argument.  If you use these, please take a look at the gpg.rc and smime.rc  
 files in the contrib subdirectory.

  - $mail\_check\_recent controls whether all unread mail or only new
    mail

 since the last mailbox visit will be reported as new.  If buffy isn't reporting what you  
 expect, try changing the value of this option.

  - If you are using GnuPG 2.1, be sure to 'set pgp\_use\_gpg\_agent'.
    Version 2.1 includes an
agent

 that is run automatically as needed.  Also, be aware that mutt is curses program: it supports  
 the curses-pinentry program, but not the tty-pinentry program.

## Upgrading from 1.4.x

  - First, be sure to take a look at the changes document at
    <http://www.mutt.org/changes.html>
