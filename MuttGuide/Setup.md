## Get a running mutt

Figure out if you have a mutt installed already by entering **"mutt
-v"** at the prompt. If you have none yet, or the displayed
configuration doesn't suit your needs, grab the source from
<http://www.mutt.org/download.html> and "do it yourself": install mutt.

In the "mutt -v" output look out for:

    System: xxx [using ncurses 5.2] [using libiconv 1.7]
    Compile options:
    -DOMAIN
    -DEBUG
    -HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
    +USE_FCNTL  -USE_FLOCK
    +USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
    +HAVE_REGCOMP  -USE_GNU_REGEX  
    +HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
    +HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
    +CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
    +ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
    +HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -HAVE_GETADDRINFO  
    -ISPELL
    SENDMAIL="/usr/lib/sendmail"
    MAILPATH="/var/mail"
    ...
    EXECSHELL="/bin/sh"

For an explanation of all the features use **./configure --help** of the
mutt source distribution. Most important are USE\_POP, USE\_IMAP &
USE\_SSL for the respective support (recommended), USE\_FCNTL &
USE\_FLOCK for the file-locking type, HOMESPOOL when delivering new mail
to home-dir instead of system spool-dir, HAVE\_COLOR for helpful
highlighting of items, and the "\[using ncurses...\]" for the library
you want mutt to use to draw its interface.

### Install

As you can see above, mutt assumes that an [MTA](MailConcept)
is already installed! So make sure you have the one you want running,
otherwise install & configure the MTA first. Note that mutt is a mail
management tool, so it has no built-in editing feature. It leaves this
up to an external editor of your choice. The editor need not be
installed prior to mutt, just remember that you will need one.

Some features like SSL support or the "Compressed-Folders" patch require
additional libs. Pre-installed "curses" libs might be outdated, or might
not provide the advanced features required to let mutt work at its
fullest capabilities (e.g. coloring). If you don't have them in place
when you install mutt, you'll have to recompile after you installed
them. But don't worry: with each update or patch you apply you have to
recompile anyway, and luckily it's not hard to do with mutt. So, just go
ahead, grab the source and follow the included install instructions.

On some systems you must use +USE\_DOTLOCK, i.e. the locking mechanism
working with the SGID "mutt\_dotlock" executable to access "inbox" mail
folders in a common system directory ("spool"), especially when the
spool-dir isn't writable for every user (for security, of course). For
this you need "root"-access to change permissions accordingly. For
installations on your private system at home *you* are "root". In a
company or other institution with a dedicated system administrator, you
better ask the staff to install mutt centrally, so everybody can use it,
not just you: "live long & prosper!"
